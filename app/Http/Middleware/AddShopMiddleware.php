<?php

namespace App\Http\Middleware;

use Closure;

class AddShopMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (\Auth::user()) {
            view()->share('shop', \Auth::user()->shop);
        }
        return $next($request);
    }
}
