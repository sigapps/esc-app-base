<?php

/**
 * Inline route handlers for testing only
 */
 
URL::forceSchema('https');
 
Route::get('/', function () {
    return view('test');
});


Route::get('app', function(\Illuminate\Http\Request $request) {
    $shop = $request->user()->shop;
    
    $lastPurchase = null;
    if ($request->get('purchase_id')) {
        $lastPurchase = $shop->getLastPurchase();
    }
    
    $plans = \Esc\Billing\Plan::all();
    
    return view('dashboard', [
        'plans' => $plans,
        'last_purchase' => $lastPurchase
    ]);
});

Route::post('app/switch-plan', function(\Illuminate\Http\Request $request) {
    $shop = $request->user()->shop;
    $plan = \Esc\Billing\Plan::find($request->get('plan_id'));
    
    $subscription = $shop->switchPlan($plan, 'app', true);
    return $subscription->redirect();
});

Route::post('app/pay-ten', function(\Illuminate\Http\Request $request) {
    $shop = $request->user()->shop;
    
    $redirect = \URL::to('/app');
    
    try {
        $purchase = $shop->createPurchase(1000, '$10 Test purchase', $redirect, true);
        if ($purchase->canPay()) { 
            return $purchase->redirect();
        }
    } catch(\Exception $ex) {}
    return redirect()->back()->with('error', 'Couldn\'t create purchase.');
});

Route::post('app/pay-twenty', function(\Illuminate\Http\Request $request) {
    $shop = $request->user()->shop;
    
    $redirect = \URL::to('/app');
    
    try {
        $purchase = $shop->createPurchase(2000, '$20 Test purchase', $redirect, true);
        if ($purchase->canPay()) { 
            return $purchase->redirect();
        }
    } catch(\Exception $ex) {}
    return redirect()->back()->with('error', 'Couldn\'t create purchase.');
});



Route::post('app/tokens-ten', function(\Illuminate\Http\Request $request) {
    $shop = $request->user()->shop;
    
    $shop->consumeTokens(10, 'Test token usage (10)');
    
    return redirect()->back();
});

Route::post('app/tokens-one', function(\Illuminate\Http\Request $request) {
    $shop = $request->user()->shop;
    
    $r = $shop->consumeTokens(1, 'Test token usage (1)');
    
    return redirect()->back();
});

Route::post('app/usage-ten', function(\Illuminate\Http\Request $request) {
    $shop = $request->user()->shop;
    
    $shop->recordUsage(10, 'Test usage (10)');

    
    return redirect()->back();
});

Route::post('app/usage-one', function(\Illuminate\Http\Request $request) {
    $shop = $request->user()->shop;
    
    $shop->recordUsage(1, 'Test usage (1)');
    
    return redirect()->back();
});