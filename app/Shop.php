<?php
namespace App;

use Esc\Shopify\Models\Shop as ShopBase;
use Esc\Billing\Billable;

class Shop extends ShopBase {
    use Billable;
    
    protected $table = 'shops';

    
}