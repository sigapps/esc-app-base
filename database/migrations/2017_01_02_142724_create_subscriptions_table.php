<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubscriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \Schema::create('subscriptions', function($table) {
            $table->increments('id');
            $table->timestamps();
            
            $table->boolean('setting_up')->default(true);
            $table->boolean('active')->default(false);
            $table->boolean('test')->default(false);
            
            $table->integer('shop_id')->unsigned();
            $table->integer('plan_id')->unsigned();
            
            $table->string('redirect');
            $table->string('status')->default('pending');
            $table->string('confirmation_url')->nullable();
            $table->string('shopify_charge_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \Schema::drop('subscriptions');
    }
}
