## Esc Shopify App Base

This project serves as a base for our apps. To create a new project in the folder `myproject`, run:

```
composer create-project esc/app-base myproject
```

### Components

This project contains the following:

- Laravel 5.1
- Esc/Shopify (provides API functions)
- Esc/Billing (provides billing helpers)


### Getting started

After creating the project, following these steps to get set up:

- Edit `.env` with your app credentials and database information.
- Edit `config/esc_shopify.php` to add your required OAuth scopes, webhooks, and script tags.
- Visit the site to view the test dashboard.
- Enter your dev shop and run the tests before continuing development.